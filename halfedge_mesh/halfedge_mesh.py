import sys
from . import config
import math
import functools
import colorsys

# python3 compatibility
try:
    xrange
except NameError:
    xrange = range
try:
    dict.iteritems
except AttributeError:
    # Python 3
    def itervalues(d):
        return iter(d.values())
    def iteritems(d):
        return iter(d.items())
else:
    # Python 2
    def itervalues(d):
        return d.itervalues()
    def iteritems(d):
        return d.iteritems()


# TODO: Reorder functions

class HalfedgeMesh:

    def __init__(self, filename=None, vertices=[], halfedges=[], facets=[]):
        """Make an empty halfedge mesh.

           filename   - a string that holds the directory location and name of
               the mesh
            vertices  - a list of Vertex types
            halfedges - a list of HalfEdge types
            facets    - a list of Facet types
        """

        self.vertices = vertices
        self.halfedges = halfedges
        self.facets = facets
        self.filename = filename
        # dictionary of all the edges given indexes
        # TODO: Figure out if I need halfedges or if I should just use edges
        # Which is faster?
        self.edges = None

        if filename:
            self.vertices, self.halfedges, self.facets, self.edges = \
                    self.read_file(filename)

    def __eq__(self, other):
        return (isinstance(other, type(self)) and 
            (self.vertices, self.halfedges, self.facets) ==
            (other.vertices, other.halfedges, other.facets))

    def __hash__(self):
        return (hash(str(self.vertices)) ^ hash(str(self.halfedges)) ^ hash(str(self.facets)) ^ 
            hash((str(self.vertices), str(self.halfedges), str(self.facets))))

    def parcoursLargeur(self, s, nb):
        if s.visited == 0:
            s.visited = nb
            ouverts = [s]
            while len(ouverts) != 0:
                s1 = ouverts.pop()
                listeVoisin = s1.getListeVoisin()
                for s2 in listeVoisin:
                    if s2.visited == 0:
                        s2.visited = nb
                        ouverts.append(s2)

    def parcoursLargeurFace(self, f, nb):
        if f.visited == 0:
            f.visited = nb
            ouverts = [f]
            while len(ouverts) != 0:
                f1 = ouverts.pop()
                listeVoisin = f.getVoisin()
                for f2 in listeVoisin:
                    if f2.visited == 0:
                        f2.visited = nb
                        ouverts.append(f2)

    def calculAngleDiedral(self):
        for f in self.facets:
            f.valeur = f.halfedge.get_angle_normal()

#          Voisin des Voisins (donc rayon 3*3 = 9 triangles)          #
#        for f in self.facets:
#            somme = 0
#            liste = f.getVoisinDesVoisins()
#            for voisin in liste:
#               somme += voisin.valeur
#            somme += f.valeur
#            f.valeur = somme/len(liste)       


#           Uniquement les voisins (donc 3 triangles)                 #
        for f in self.facets:
            somme = 0
            liste = f.getVoisin()
            for voisin in liste:
                somme = somme + voisin.valeur
            somme = somme + f.valeur
            f.valeur = somme/3

        
    def getMinAngleDiedral(self):
        minimal = self.facets[0].valeur
        for f in self.facets:
            if minimal > f.valeur:
                minimal = f.valeur
        return minimal 

    def getMaxAngleDiedral(self):
        maximal = 0
        for f in self.facets:
            if maximal < f.valeur:
                maximal = f.valeur
        return maximal

    def faceConnexe(self):
        for f in self.facets:
            f.visited = 0
        ans = 0
        for s in self.facets:
            if f.visited == 0:
                self.parcoursLargeurFace(f, ans+1)
                ans += 1
        return ans

    def nombreConnexe(self):
        for v in self.vertices:
            v.visited = 0
        ans = 0
        for s in self.vertices:
            if s.visited == 0:
                # Parcours en largeur 
                self.parcoursLargeur(s, ans+1)
                ans += 1
        return ans

    def genre(self):
        # Il nous faut le nombre de sommets, d'arrêtes et de faces
        # Sommets - Arrêtes + faces 
        nbrCC = self.nombreConnexe()
        genre = [] 
        
        for i in range(1, nbrCC+1):    
            nbrSommets = len([x for x in self.vertices if x.visited == i])
            nbrFaces = len([x for x in self.facets if x.halfedge.vertex.visited == i])
            nbrArrete = len([x for x in self.halfedges if x.vertex.visited == i])/2
            X = nbrSommets - nbrArrete + nbrFaces
            genre.append((2 - X) / 2)
        
        return genre

    def get_distance(self, idVertex):        
        # On setup les distances au début de l'algo 
        distance = [-1 for i in self.vertices]
        distance[idVertex] = 0

        # On définit le prédécesseur de chaque sommet
        pred = [-1 for i in self.vertices]

        Q = [idVertex]
        while len(Q) != 0:
           # Trouver le point Q de distance minimale (et son index grâce à enumerate)
           idmin, pmin = min(enumerate(Q), key = lambda x: distance[x[1]])
           v = self.vertices[pmin] 

           # Enlever ce point de Q
           del Q[idmin] 
           

           # Pour chacun de ses voisins, mettre à jour la distance et les ajouter à Q s'ils ont 
           # été mis à jour 
           e = v.halfedge
           first = True
           while first or e != v.halfedge:
               # On récupère le voisin 
                vv = e.opposite.vertex
                # On calcule la distance entre v et nb
                nb = vv.index
                edge_distance = vv.distance_to(v) 

                # On calcule la nouvelle distance a la source de nb
                nouvelle_distance = distance[pmin] + edge_distance 

                # Si cette distance est plus courte que la distance existante, on met a jour 
                # pred[] et distance[], et on ajoute ce voisin    
                ancienne_distance = distance[nb]
                if ancienne_distance < 0 or nouvelle_distance < ancienne_distance:
                    pred[nb] = pmin
                    distance[nb] = nouvelle_distance
                    Q.append(nb) 

                e = e.next_around_vertex()
                first = False

        return distance 
           


    def read_file(self, filename):
        """Determine the type of file and use the appropriate parser.

        Returns a HalfedgeMesh
        """
        try:
            with open(filename, 'r') as file:

                first_line = file.readline().strip().upper()

                if first_line != "OFF":
                    raise ValueError("Filetype: " + first_line + " not accepted")

                # TODO: build OBJ, PLY parsers
                parser_dispatcher = {"OFF": self.parse_off}
                                      
                return parser_dispatcher[first_line](file)

        except IOError as e:
            print("I/O error({0}): {1}".format(e.errno, e.strerror))
            return
        except ValueError as e:
            print("Value error: {0}:".format(e))
            return

    def read_off_vertices(self, file_object, number_vertices):
        """Read each line of the file_object and return a list of Vertex types.
        The list will be as [V1, V2, ..., Vn] for n vertices

        Return a list of vertices.
        """
        vertices = []

        # Read all the vertices in
        for index in xrange(number_vertices):
            line = file_object.readline().split()

            try:
                # convert strings to floats
                line = list(map(float, line))
            except ValueError as e:
                raise ValueError("vertices " + str(e))

            vertices.append(Vertex(line[0], line[1], line[2], index))

        return vertices

    def parse_build_halfedge_off(self, file_object, number_facets, vertices):
        """Link to the code:
        http://stackoverflow.com/questions/15365471/initializing-half-edge-
        data-structure-from-vertices

        Pseudo code:
        map< pair<unsigned int, unsigned int>, HalfEdge* > Edges;

        for each face F
        {
            for each edge (u,v) of F
            {
                Edges[ pair(u,v) ] = new HalfEdge();
                Edges[ pair(u,v) ]->face = F;
            }
            for each edge (u,v) of F
            {
                set Edges[ pair(u,v) ]->nextHalfEdge to next half-edge in F
                if ( Edges.find( pair(v,u) ) != Edges.end() )
                {
                    Edges[ pair(u,v) ]->oppositeHalfEdge = Edges[ pair(v,u) ];
                    Edges[ pair(v,u) ]->oppositeHalfEdge = Edges[ pair(u,v) ];
            }
        }

        """
        Edges = {}
        facets = []
        halfedge_count = 0
        #TODO Check if vertex index out of bounds

        # For each facet
        for index in xrange(number_facets):
            line = file_object.readline().split()

            # convert strings to ints
            line = list(map(int, line))

            # TODO: make general to support non-triangular meshes
            # Facets vertices are in counter-clockwise order
            facet = Facet(line[1], line[2], line[3], index)
            facets.append(facet)

            # create pairing of vertices for example if the vertices are
            # verts = [1,2,3] then zip(verts, verts[1:]) = [(1,2),(2,3)]
            # note: we skip line[0] because it represents the number of vertices
            # in the facet.
            all_facet_edges = list(zip(line[1:], line[2:]))
            all_facet_edges.append((line[3], line[1]))

            # For every halfedge around the facet
            for i in xrange(3):
                Edges[all_facet_edges[i]] = Halfedge()
                Edges[all_facet_edges[i]].facet = facet
                Edges[all_facet_edges[i]].vertex = vertices[
                    all_facet_edges[i][1]]
                vertices[all_facet_edges[i][1]].halfedge = Edges[all_facet_edges[i]]
                halfedge_count +=1

            facet.halfedge = Edges[all_facet_edges[0]]

            for i in xrange(3):
                Edges[all_facet_edges[i]].next = Edges[
                    all_facet_edges[(i + 1) % 3]]
                Edges[all_facet_edges[i]].prev = Edges[
                    all_facet_edges[(i - 1) % 3]]

                # reverse edge ordering of vertex, e.g. (1,2)->(2,1)
                if all_facet_edges[i][2::-1] in Edges:
                    Edges[all_facet_edges[i]].opposite = \
                        Edges[all_facet_edges[i][2::-1]]

                    Edges[all_facet_edges[i][2::-1]].opposite = \
                        Edges[all_facet_edges[i]]

        return facets, Edges

    def parse_off(self, file_object):
        """Parses OFF files

        Returns a HalfedgeMesh
        """
        facets, halfedges, vertices = [], [], []

        # TODO Make ability to discard # lines
        vertices_faces_edges_counts = list(map(int, file_object.readline().split()))

        number_vertices = vertices_faces_edges_counts[0]
        vertices = self.read_off_vertices(file_object, number_vertices)

        number_facets = vertices_faces_edges_counts[1]
        facets, Edges = self.parse_build_halfedge_off(file_object,
                                                      number_facets, vertices)

        i = 0
        for key, value in iteritems(Edges):
            value.index = i
            halfedges.append(value)
            i += 1

        return vertices, halfedges, facets, Edges

    def get_halfedge(self, u, v):
        """Retrieve halfedge with starting vertex u and target vertex v

        u - starting vertex
        v - target vertex

        Returns a halfedge
        """
        return self.edges[(u, v)]

    def update_vertices(self, vertices):
        # update vertices
        vlist = []
        i = 0
        for v in vertices:
            vlist.append(Vertex(v[0], v[1], v[2], i))
            i += 1
        self.vertices = vlist

        hlist = []
        # update all the halfedges
        for he in self.halfedges:
            vi = he.vertex.index
            hlist.append(Halfedge(None, None, None, self.vertices[vi], None,
                he.index))

        flist = []
        # update neighboring halfedges
        for f in self.facets:
            hi = f.halfedge.index
            flist.append(Facet(f.a, f.b, f.c, f.index,  hlist[hi]))
        self.facets = flist


        i = 0
        for he in self.halfedges:
            nextid = he.next.index
            oppid = he.opposite.index
            previd = he.prev.index

            hlist[i].next = hlist[nextid]
            hlist[i].opposite = hlist[oppid]
            hlist[i].prev = hlist[previd]


            fi = he.facet.index
            hlist[i].facet = flist[fi]
            i += 1

        self.halfedges = hlist

    def hadAngleDiedral(self):
        for v in self.facets:
            if v.valeur == None:
                return False
        return True

    def getSeuilVousMeme(self):
        seuil = input("Quelle est le seuil que vous voulez mettre ?\n")
        return float(seuil)

    def getMoyenne(self):
        somme = 0
        count = 0
        for f in self.facets:
            somme += f.valeur
            count = count + 1

        return math.ceil(somme/count)

    def getMediane(self):
        liste = []
        count = 0
        for f in self.facets:
            liste.append(f.valeur)
            count = count + 1
        liste.sort()
        return liste[math.floor(count/2)]
    
    def getPremierQuartile(self):
        liste = []
        count = 0
        for f in self.facets:
            liste.append(f.valeur)
            count += 1
        liste.sort()
        return liste[math.floor(count/4)*3]


    def getSeuil(self):
        version = input("Quelle version de seuil voulez-vous ? \n 1 - Déterminez vous-même le seuil \n 2 - Moyenne \n 3 - Médiane \n 4 - 1er Quartile\n")
        version = int(version)
        if(version < 5 and version > 0):
            if(version == 1):
                seuil = self.getSeuilVousMeme()
            if(version == 2):
                seuil = self.getMoyenne()
            if(version == 3):
                seuil = self.getMediane()
            else:
                seuil = self.getPremierQuartile()
        return seuil

    def setClasse(self, seuil):
        n = self.faceConnexe()
        for f in self.facets:
            if f.valeur > seuil:
                f.classe = 1 + f.visited
            else:
                f.classe = 0 + f.visited
        return 2*n+1



    def save_off(self, filename, distances_sommets = [], angleDiedral = []):
        with_diedral = self.hadAngleDiedral()
        with_sommets = len(distances_sommets) == len(self.vertices)
        if with_sommets:
            min_d = min([x for x in distances_sommets if x >= 0])
            max_d = max(distances_sommets)
        if with_diedral:
            min_angle = self.getMinAngleDiedral()
            max_angle = self.getMaxAngleDiedral()
            seuil = self.getSeuil()
            N = self.setClasse(seuil)
        try :
            with open(filename, "w") as file:
                if with_sommets or with_diedral:
                    file.write("C")
                file.write("OFF\n")
                
                # witre number of vertices, faces and edges
                file.write(str(len(self.vertices)) + " " + str(len(self.facets)) + " 0\n")
                 
                # for each vertex write its coordinate
                for v in self.vertices:
                    file.write(str(v.x) + " " + str(v.y) + " " + str(v.z))
                    if with_sommets:
                        if distances_sommets[v.index] < 0:
                            file.write(' 255 255 0')
                        else: 
                            v = int(255 * ((distances_sommets[v.index] - min_d) / (max_d - min_d)))    
                            file.write(' 255 ' + str(v) + ' ' + str(v)) 
                    file.write("\n")
                    

                #for each face write its description (number of vertices and 
                # a list of vertex index)  
                for f in self.facets:
                    file.write("3 " + str(f.a) + " " + str(f.b) + " " + str(f.c))
                    if with_diedral:
                        HSV_tuples = [(x*1.0/N, 0.5, 0.5) for x in range(N)]
                        RGB_tuples = map(lambda x: colorsys.hsv_to_rgb(*x), HSV_tuples)
                        rgb = list(RGB_tuples)
                        file.write(' ' + str(rgb[f.classe][0]) + " " + str(rgb[f.classe][1]) + " " + str(rgb[f.classe][2]) + " 1.0")
                        
                        file.write("\n")

        except IOError as e:
            print("I/O error({0}): {1}".format(e.erno, e.strerror)) 
            return
        except ValueError as e:
            print("Value error: {0}:".format(e))
            return

class Vertex:

    def __init__(self, x=0, y=0, z=0, index=None, halfedge=None):
        """Create a vertex with given index at given point.

        x        - x-coordinate of the point
        y        - y-coordinate of the point
        z        - z-coordinate of the point
        index    - integer index of this vertex
        halfedge - a halfedge that points to the vertex
        """

        self.x = x
        self.y = y
        self.z = z

        self.index = index

        self.halfedge = halfedge

    def __eq__(x, y):
        return x.__key() == y.__key() and type(x) == type(y)

    def __key(self):
        return (self.x, self.y, self.z, self.index)

    def __hash__(self):
        return hash(self.__key())

    def get_vertex(self):
        return [self.x, self.y, self.z]

    def distance_to(self, v):
        return norm([vv2 - vv1 for vv1, vv2 in zip(self.get_vertex(), v.get_vertex())])

    def getListeVoisin(s):
        base = s.halfedge
        liste = []
        suivant = base.next_around_vertex()
        while  suivant != base:
            liste.append(suivant.opposite.vertex) #append le sommet de suivant
            suivant = suivant.next_around_vertex() 
        return liste

class Facet:

    def __init__(self, a=-1, b=-1, c=-1, index=None, halfedge=None, valeur=None):
        """Create a facet with the given index with three vertices.

        a, b, c - indices for the vertices in the facet, counter clockwise.
        index - index of facet in the mesh
        halfedge - a Halfedge that belongs to the facet
        """
        self.a = a
        self.b = b
        self.c = c
        self.index = index
        # halfedge going ccw around this facet.
        self.halfedge = halfedge
        self.valeur = valeur 

    def __eq__(self, other):
        return self.a == other.a and self.b == other.b and self.c == other.c \
            and self.index == other.index and self.halfedge == other.halfedge and self.valeur == other.valeur

    def __hash__(self):
        return hash(self.halfedge) ^ hash(self.a) ^ hash(self.b) ^ \
            hash(self.c) ^ hash(self.index) ^ \
            hash((self.halfedge, self.a, self.b, self.c, self.index))

    def get_normal(self):
        """Calculate the normal of facet

        Return a python list that contains the normal
        """
        vertex_a = [self.halfedge.vertex.x, self.halfedge.vertex.y,
                    self.halfedge.vertex.z]

        vertex_b = [self.halfedge.next.vertex.x, self.halfedge.next.vertex.y,
                    self.halfedge.next.vertex.z]

        vertex_c = [self.halfedge.prev.vertex.x, self.halfedge.prev.vertex.y,
                    self.halfedge.prev.vertex.z]

        # create edge 1 with vector difference
        edge1 = [u - v for u, v in zip(vertex_b, vertex_a)]
        edge1 = normalize(edge1)
        # create edge 2 ...
        edge2 = [u - v for u, v in zip(vertex_c, vertex_b)]
        edge2 = normalize(edge2)

        # cross product
        normal = cross_product(edge1, edge2)

        normal = normalize(normal)

        return normal

    def getVoisin(self):
        liste = []
        base = self.halfedge
        suivant = self.halfedge.next
        while suivant != base:
            liste.append(suivant.opposite.facet)
            suivant = suivant.next
        return liste

    def getVoisinDesVoisins(self):
        listeVoisin = self.getVoisin()
        liste = []
        for f in listeVoisin:
            liste.append(f)
            liste.extend(f.getVoisin())

        return liste


class Halfedge:

    def __init__(self, next=None, opposite=None, prev=None, vertex=None,
                 facet=None, index=None):
        """Create a halfedge with given index.
        """
        self.opposite = opposite
        self.next = next
        self.prev = prev
        self.vertex = vertex
        self.facet = facet
        self.index = index

    def __eq__(self, other):
        # TODO Test more
        return (self.vertex == other.vertex) and \
               (self.prev.vertex == other.prev.vertex) and \
               (self.index == other.index)

    def __hash__(self):
        return hash(self.opposite) ^ hash(self.next) ^ hash(self.prev) ^ \
                hash(self.vertex) ^ hash(self.facet) ^ hash(self.index) ^ \
                hash((self.opposite, self.next, self.prev, self.vertex,
                    self.facet, self.index))

    def next_around_vertex(self):
        return self.next.opposite

    def get_angle_normal(self):
        """Calculate the angle between the normals that neighbor the edge.

        Return an angle in radians
        """
        a = self.facet.get_normal()
        b = self.opposite.facet.get_normal()

        dir = [self.vertex.x - self.prev.vertex.x,
               self.vertex.y - self.prev.vertex.y,
               self.vertex.z - self.prev.vertex.z]
        dir = normalize(dir)

        ab = dot(a, b)

        args = ab / (norm(a) * norm(b))

        if allclose(args, 1):
            args = 1
        elif allclose(args, -1):
            args = -1

        assert (args <= 1.0 and args >= -1.0)

        angle = math.acos(args)

        if not (angle % math.pi == 0):
            e = cross_product(a, b)
            e = normalize(e)

            vec = dir
            vec = normalize(vec)

            if (allclose(vec, e)):
                return angle
            else:
                return -angle
        else:
            return 0


def allclose(v1, v2):
    """Compare if v1 and v2 are close

    v1, v2 - any numerical type or list/tuple of numerical types

    Return bool if vectors are close, up to some epsilon specified in config.py
    """

    v1 = make_iterable(v1)
    v2 = make_iterable(v2)

    elementwise_compare = list(map(
        (lambda x, y: abs(x - y) < config.EPSILON), v1, v2))
    return functools.reduce((lambda x, y: x and y), elementwise_compare)


def make_iterable(obj):
    """Check if obj is iterable, if not return an iterable with obj inside it.
    Otherwise just return obj.

    obj - any type

    Return an iterable
    """
    try:
        iter(obj)
    except:
        return [obj]
    else:
        return obj


def dot(v1, v2):
    """Dot product(inner product) of v1 and v2

    v1, v2 - python list

    Return v1 dot v2
    """
    elementwise_multiply = list(map((lambda x, y: x * y), v1, v2))
    return functools.reduce((lambda x, y: x + y), elementwise_multiply)


def norm(vec):
    """ Return the Euclidean norm of a 3d vector.

    vec - a 3d vector expressed as a list of 3 floats.
    """
    return math.sqrt(functools.reduce((lambda x, y: x + y * y), vec, 0.0))

def distance(v1 , v2):
    return norm([vv2 - vv1 for vv1, vv2 in zip(v1.get_vertex(), v2.get_vertex())])

def normalize(vec):
    """Normalize a vector

    vec - python list

    Return normalized vector
    """
    if norm(vec) < 1e-6:
        return [0 for i in xrange(len(vec))]
    return list(map(lambda x: x / norm(vec), vec))


def cross_product(v1, v2):
    """ Return the cross product of v1, v2.

    v1, v2 - 3d vector expressed as a list of 3 floats.
    """
    x3 = v1[1] * v2[2] - v2[1] * v1[2]
    y3 = -(v1[0] * v2[2] - v2[0] * v1[2])
    z3 = v1[0] * v2[1] - v2[0] * v1[1]
    return [x3, y3, z3]

def create_vector(p1, p2):
    """Contruct a vector going from p1 to p2.

    p1, p2 - python list wth coordinates [x,y,z].

    Return a list [x,y,z] for the coordinates of vector
    """
    return list(map((lambda x,y: x-y), p2, p1))
