import halfedge_mesh
from halfedge_mesh.halfedge_mesh import distance

# .off are supported
mesh = halfedge_mesh.HalfedgeMesh("Mesh/horse.off")

#print(mesh.nombreConnexe())

if mesh.nombreConnexe() == 1:
    distances = mesh.get_distance(0)
    mesh.save_off("Mesh/testEcriture.off", distances)

mesh.calculAngleDiedral()

mesh.save_off("sortie.off")
